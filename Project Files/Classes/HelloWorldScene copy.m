//
//  HelloWorldLayer.m
//  Menu
//
//  Created by Greg Roberts on 8/21/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

// Import the interfaces
#import "help.h"
#import "HelloWorldScene.h"
#import "play.h"
#import "classic.h"
#import "progressive.h"
#import "load.h"
// HelloWorld implementation
@implementation HelloWorld

+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorld *layer = [HelloWorld node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init
{
	sleep(2);
	if( (self=[super init] )) {
		
		CCSprite* background = [CCSprite spriteWithFile:@"main.png"];
		background.tag = 1;
		background.anchorPoint = CGPointMake(0, 0);
		[self addChild:background];
		
		CCMenuItemImage *item1 = [CCMenuItemImage itemFromNormalImage:@"playoff.png" selectedImage:@"playon.png"
															   target:self
															 selector:@selector(item1Action:)];
		
		
		CCMenuItemImage *item2 = [CCMenuItemImage itemFromNormalImage:@"optionsoff.png" selectedImage:@"optionson.png"
															   target:self
															 selector:@selector(item2Action:)];
		
		
		
		CCMenu *menu = [CCMenu menuWithItems:item1,item2, nil];
		[menu alignItemsVertically];
		
		[self addChild:menu];
		
	}
	return self;
	
}	

-(void)item1Action:(id)sender{
	[[CCDirector sharedDirector]replaceScene:[CCZoomFlipXTransition transitionWithDuration:1 scene: [play node]]];
}

-(void)item2Action:(id)sender{
	[[CCDirector sharedDirector]replaceScene:[CCZoomFlipXTransition transitionWithDuration:1 scene: [help node]]];
}
@end