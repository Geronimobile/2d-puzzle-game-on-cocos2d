//
//  HelloWorldLayer.m
//  Menu
//
//  Created by Greg Roberts on 8/21/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

// Import the interfaces
#import "help.h"
#import "HelloWorldScene.h"
#import "play.h"
#import "classic.h"
//#import "progressive.h"
#import "load.h"
// HelloWorld implementation
@implementation HelloWorld

+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorld *layer = [HelloWorld node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init
{
    //sleep(2)
	if( (self=[super init] )) {
    
        int offset;
        
        if([[[CCDirector sharedDirector] openGLView] window].frame.size.height == 320) offset = 0;
        else offset = ( [[[CCDirector sharedDirector] openGLView] window].frame.size.height - 480 ) / 2;
        
        CCSprite* background = [CCSprite spriteWithFile:@"backgroundmain.png"];
        background.tag = 1;
        background.anchorPoint = CGPointMake(0, 0);
        background.position = ccp(0,offset);
        [self addChild:background];
          
		CCMenuItemImage *item1;
        
item1 = [CCMenuItemImage itemFromNormalImage:@"play1.png" selectedImage:@"play1.png" target:self selector:@selector(item1Action:)];
         
        CCMenuItemImage *item2 = [CCMenuItemImage itemFromNormalImage:@"meet1.png" selectedImage:@"meet1.png" target:self selector:@selector(settings:)];

		CCMenuItemImage *fb = [CCMenuItemImage itemFromNormalImage:@"f1.png" selectedImage:@"f1.png" target:self selector:@selector(facebook:)];

        CCMenuItemImage *tw = [CCMenuItemImage itemFromNormalImage:@"fb1.png" selectedImage:@"fb1.png" target:self selector:@selector(twitter:)];
        
        CCMenuItemImage *del = [CCMenuItemImage itemFromNormalImage:@"instructions.png" selectedImage:@"instructions" target:self selector:@selector(del:)];

        CCMenu *menu = [CCMenu menuWithItems:item1,item2, fb, tw, del, nil];
        [menu alignItemsVertically];
        
        item1.position = ccp(0,10);
        item2.position = ccp(0,-100);
        del.position = ccp(0,-45);
        
        fb.position = ccp(-120,-210);
        
        tw.position = ccp(-65,-210);
        
		[self addChild:menu];
		
	}
	return self;
	
}	

-(void) alertView1:(UIAlertView *)alert clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex == 1){
        UIAlertView *final = [[UIAlertView alloc] init];
        [final setDelegate:self];
        [final setTitle:@"How To Play"];
        [final setMessage:@"This GloFish® App is level based, with each level challenging you to uncover a brilliant GloFish® fluorescent fish image. Start each level by moving the tile around the screen, revealing the image as the tile moves. You cannot move the tile over the same space twice and you must completely uncover the image to complete the level."];
        [final addButtonWithTitle:@"Let's Play!"];
        [final show];
        [final release];
    }
    
}

-(void) alertView2:(UIAlertView *)final clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 1){
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:1] forKey:@"Level"];
    }
}

-(void)item1Action:(id)sender{
	[[CCDirector sharedDirector]replaceScene:[CCTransitionFlipX transitionWithDuration:1 scene: [classic node]]];
}

-(void)facebook:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.facebook.com/glofish"]];
}

-(void)del:(id)sender{
    
    UIAlertView *final = [[UIAlertView alloc] init];
    [final setDelegate:self];
    [final setTitle:@"How To Play"];
    [final setMessage:@"This GloFish® App is level based, with each level challenging you to uncover a brilliant GloFish® fluorescent fish image. Start each level by moving the tile around the screen, revealing the image as the tile moves. You cannot move the tile over the same space twice and you must completely uncover the image to complete the level."];
    [final addButtonWithTitle:@"Let's Play!"];
    [final show];
    [final release];
    
}

-(void)twitter:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://twitter.com/GloFish"]];
}

-(void)settings:(id)sender{
[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.glofish.com/meet-glofish/glofish-gallery/"]];
}

@end