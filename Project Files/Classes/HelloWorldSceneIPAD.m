//
//  HelloWorldLayer.m
//  Menu
//
//  Created by Greg Roberts on 8/21/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

// Import the interfaces
#import "help.h"
#import "HelloWorldSceneIPAD.h"
#import "play.h"
#import "classicIPAD.h"
//#import "progressive.h"
#import "load.h"
// HelloWorld implementation
@implementation HelloWorldIPAD

+(id) scene
{
	CCScene *scene = [CCScene node];
	HelloWorldIPAD *layer = [HelloWorldIPAD node];
	[scene addChild: layer];
	return scene;
}

-(id) init
{
	if( (self=[super init] )) {
        
        CCSprite* background = [CCSprite spriteWithFile:@"ipad.png"];
        background.anchorPoint = CGPointMake(0, 0);
        [self addChild:background];
        
		CCMenuItemImage *item1 = [CCMenuItemImage itemFromNormalImage:@"play1IPAD.png" selectedImage:@"play1IPAD.png" target:self selector:@selector(item1Action:)];
            
        CCMenuItemImage *item2 = [CCMenuItemImage itemFromNormalImage:@"meetIPAD.png" selectedImage:@"meetIPAD.png" target:self selector:@selector(settings:)];

		CCMenuItemImage *fb = [CCMenuItemImage itemFromNormalImage:@"f3.png" selectedImage:@"f3.png" target:self selector:@selector(facebook:)];

        CCMenuItemImage *tw = [CCMenuItemImage itemFromNormalImage:@"fb3.png" selectedImage:@"fb3.png" target:self selector:@selector(twitter:)];

        CCMenuItemImage *settings = [CCMenuItemImage itemFromNormalImage:@"instructionsIPAD.png" selectedImage:@"instructionsIPAD.png" target:self selector:@selector(instr:)];
        
        CCMenu *menu = [CCMenu menuWithItems:item1,item2, settings, fb, tw, nil];
        [menu alignItemsVertically];
        
        item1.position = ccp(0,30);
        item2.position = ccp(0,-200);
        settings.position = ccp(0,-90);
        
        fb.position = ccp(-310,-435);
        
        tw.position = ccp(-170,-435);
		
		[self addChild:menu];
		
	}
	return self;
	
}	

-(void)item1Action:(id)sender{
	[[CCDirector sharedDirector]replaceScene:[CCTransitionFlipX transitionWithDuration:1 scene: [classicIPAD node]]];
}

-(void)facebook:(id)sender{
[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.facebook.com/glofish"]];
}

-(void)twitter:(id)sender{
[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://twitter.com/GloFish"]];	
}

-(void)settings:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.glofish.com/meet-glofish/glofish-gallery/"]];
}

-(void)instr:(id)sender{
   
    UIAlertView *final = [[UIAlertView alloc] init];
    [final setDelegate:self];
    [final setTitle:@"How To Play"];
    [final setMessage:@"This GloFish® App is level based, with each level challenging you to uncover a brilliant GloFish® fluorescent fish image. Start each level by moving the tile around the screen, revealing the image as the tile moves. You cannot move the tile over the same space twice and you must completely uncover the image to complete the level."];
    [final addButtonWithTitle:@"Let's Play!"];
    [final show];
    [final release];
    
}

@end