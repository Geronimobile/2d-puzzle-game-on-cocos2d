

#import <UIKit/UIKit.h>

@interface MenuAppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow *window;
}

@property (nonatomic, retain) UIWindow *window;

@end
