//
//  MenuAppDelegate.m
//  Menu
//
//  Created by Greg Roberts on 8/21/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "MenuAppDelegate.h"
#import "cocos2d.h"
#import "HelloWorldScene.h"
#import "HelloWorldSceneIPAD.h"

@implementation MenuAppDelegate

@synthesize window;

- (void) applicationDidFinishLaunching:(UIApplication*)application
{

	CC_DIRECTOR_INIT();
	
	CCDirector *director = [CCDirector sharedDirector];
	
	[director setDeviceOrientation:kCCDeviceOrientationPortrait];
	
	[director setDisplayFPS:YES];
	
	EAGLView *view = [director openGLView];
    
	[view setMultipleTouchEnabled:YES];
	
    [director enableRetinaDisplay:YES];
    
	// Default texture format for PNG/BMP/TIFF/JPEG/GIF images
	// It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
	// You can change anytime.
	[CCTexture2D setDefaultAlphaPixelFormat:kTexture2DPixelFormat_RGBA8888];	
	
    
    [director setOpenGLView:view];
    
    // Enables High Res mode (Retina Display) on iPhone 4 and maintains low res on all other devices
    if( ! [director enableRetinaDisplay:YES] )  CCLOG(@"Retina Display Not supported");
    
    [director setProjection:kCCDirectorProjection2D];
    
	[[CCDirector sharedDirector] setDisplayFPS:NO];
    
    if([[[CCDirector sharedDirector] openGLView] window].frame.size.height >= 1024) [[CCDirector sharedDirector] runWithScene: [HelloWorldIPAD scene]];
    else [[CCDirector sharedDirector] runWithScene: [HelloWorld scene]];
    
}

//- (void)applicationdidFinishLaunchingWithOptions:(UIApplication *)application {
//	[CCDirector setProjection:kCCDirectorProjection2D];
//}

- (void)applicationWillResignActive:(UIApplication *)application {
	[[CCDirector sharedDirector] pause];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	[[CCDirector sharedDirector] resume];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
	[[CCDirector sharedDirector] purgeCachedData];
}

-(void) applicationDidEnterBackground:(UIApplication*)application {
	[[CCDirector sharedDirector] stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application {
	[[CCDirector sharedDirector] startAnimation];
}

- (void)applicationWillTerminate:(UIApplication *)application {
	[[CCDirector sharedDirector] end];
}

- (void)applicationSignificantTimeChange:(UIApplication *)application {
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}

- (void)dealloc {
	[[CCDirector sharedDirector] release];
	[window release];
	[super dealloc];
}

@end
