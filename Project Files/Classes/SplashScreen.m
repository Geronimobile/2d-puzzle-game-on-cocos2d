//
//  SplashScreen.m
//
//  Created by Dmitri Fedortchenko on 2010-04-23.
//  Use freely.
//

#import "SplashScreen.h"
#import "MenuLayer.h"
#import "RemoveFromParentAction.h"

#define kSplashScreenDisplayTime 2.f
#define kTouchPrioritySplashScreen INT_MIN+1

@interface SplashScreen(Private)
- (void)popSplash;
@end

@implementation SplashScreen

+ (id)splashScreenWithFiles:(NSString*) item, ...
{
	va_list args;
	va_start(args,item);
	
	id s = [[[self alloc] initWithFiles:item vaList:args] autorelease];
	
	va_end(args);
	return s;
}

- (id)initWithFiles:(NSString*) item vaList: (va_list) args
{
	self = [super init];
	if (self==nil) return nil;
	
	timer_ = 0;
	minTimer_ = 0;
	
	self.anchorPoint = ccp(0.5,0.5);
	self.position = screenCenter();
	
	self.isRelativeAnchorPoint = YES;
	self.isTouchEnabled = YES;
	
	int childIndex = INT_MAX-1;
	
	CCTexture2DPixelFormat origFormat = [CCTexture2D defaultAlphaPixelFormat];
	[CCTexture2D setDefaultAlphaPixelFormat:kTexture2DPixelFormat_RGBA8888];
	
	defaultSprite_ = [CCSprite spriteWithFile:@"DefaultImg.png"];
	defaultSprite_.rotation = -90;
	defaultSprite_.anchorPoint = ccp(0.5,0.5);
	defaultSprite_.position = screenCenter();
	[self addChild:defaultSprite_  z:childIndex tag:childIndex];
	childIndex--;
	
	currentIndex = 0;
	
	if (item) {
		CCSprite* sprite = [CCSprite spriteWithFile:item];
		[sprite.texture setAliasTexParameters];
		sprite.anchorPoint = ccp(0.5,0.5);
		sprite.position = screenCenter();
		[self addChild:sprite  z:childIndex tag:childIndex];
		NSString *filename = va_arg(args, NSString*);
		while(filename) {
			childIndex--;
			CCSprite* sprite = [CCSprite spriteWithFile:filename];
			sprite.anchorPoint = ccp(0.5,0.5);
			sprite.position = screenCenter();
			[sprite.texture setAliasTexParameters];
			[self addChild:sprite z:childIndex tag:childIndex];
			filename = va_arg(args, NSString*);
			currentIndex++;
		}
	}
	
	[CCTexture2D setDefaultAlphaPixelFormat:origFormat];
	
	if (currentIndex==0)
		ignoreButAbsorbTouch = YES;
	
	self.contentSize = [[CCDirector sharedDirector] winSize];
	
	return self;
}

- (void)onEnter
{
	[self scheduleUpdate];
	
	[defaultSprite_ runAction:[CCFadeTo actionWithDuration:.5f opacity:0]];
	[super onEnter];
}
- (void)update:(ccTime)dt
{
	if (currentIndex>=0 && minTimer_ >= .5f && timer_ >= kSplashScreenDisplayTime)
	{
		timer_=0;
		minTimer_=0;
		
		[self popSplash];
	}
	timer_ +=dt;
	minTimer_+=dt;
}

- (void)popSplash
{
	timer_ = 0;
	minTimer_=0;
	
	if (currentIndex >= 0)
	{
		if ( currentIndex == 0 )
		{
			ignoreButAbsorbTouch = YES;
			[[CCDirector sharedDirector] replaceScene:[CCCrossFadeTransition transitionWithDuration:0.5f scene:[MenuLayer sceneAndResumeGameIfInProgress]]];
			[self unscheduleUpdate];
		}
		else
		{
			[self removeChild:[[self children] objectAtIndex:currentIndex] cleanup:YES];
		}
	}
	
	if (currentIndex == 1)
	{
		[MenuLayer preLoadGameData];
	}
	
	currentIndex--;
}

-(void)registerWithTouchDispatcher
{
	[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:kTouchPrioritySplashScreen swallowsTouches:YES];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
	if (!ignoreButAbsorbTouch)
		[self popSplash];
	return YES;
}

@end