//
//  classic.m
//  Menu
//
//  Created by Joe Malott on 1/18/11.
//  Copyright 2011 0. All rights reserved.
//

#import "help.h"
#import "HelloWorldScene.h"
#import "play.h"
#import "classic.h"
#import "progressive.h"

@implementation classic
int map2[10][8];
CGRect rects[10][8];
int currentX, currentY;
+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	//userInteractionEnables = true;

	// 'layer' is an autorelease object.
	classic *layer = [classic node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init
{
	if( (self=[super init] )) {

		//int _1[] = {0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,2,1,0,0,0,1};

		
		CCSprite *background = [CCSprite spriteWithFile:@"main3.png"];
		background.tag = 1;
		background.anchorPoint = CGPointMake(0, 0);
		[self addChild:background];

		}
	
	int _2[10][8] ={{-1,-1,-1,-1,-1,-1,-1,-1},
		{-1,0,1,0,0,0,1,-1},
		{-1,0,1,0,1,0,1,-1},
		{-1,2,0,0,1,0,0,-1},
		{-1,0,0,1,0,0,0,-1},
		{-1,0,1,0,0,0,2,-1},
		{-1,0,0,2,0,0,0,-1},
		{-1,0,0,0,0,0,1,-1},
		{-1,0,0,0,0,0,1,-1},
		{-1,-1,-1,-1,-1,-1,-1,-1}};
	
	
	for(int a = 0; a < 10; a++)
		for(int b = 0; b < 8; b++)
			map2[a][b] = _2[a][b];
	

	for(int yPosition = 12, a = 0, b = 0, c = 0; a<10;a++, yPosition += 48,b = 0)
		for(int xPosition = -6; b < 8; xPosition += 48, b++, c++)
		{
			if(map2[a][b] == 0)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"plain.png"];
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(map2[a][b] == 2)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"start.png"];
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
		}
	
	for(int yPosition = 12, a = 0, b = 0, c = 0; a<10;a++,b = 0, yPosition += 48)
		for(int xPosition = -6; b < 8; xPosition += 48, b++, c++)	
		{
			if(a == 9 && (map2[8][b] == 0 || map2[8][b] == 2))
			{
				backSprites[a][b] = [CCSprite spriteWithFile:@"4.png"];//down
				backSprites[a][b].position = ccp(xPosition,yPosition);
				[self addChild:backSprites[a][b]];	
			}
			else if(a == 0 && (map2[a+1][b] == 0 || map2[a+1][b] == 2))
			{
				backSprites[a][b] = [CCSprite spriteWithFile:@"2.png"];//up
				backSprites[a][b].position = ccp(xPosition,yPosition);
				[self addChild:backSprites[a][b]];	
			}
			else if(b == 0 && (map2[a][b+1] == 0 || map2[a][b+1] == 2))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"3.png"];//right
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(b == 7 && (map2[a][b-1] == 0 || map2[a][b-1] == 2))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"1.png"];//left
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if((map2[a][b] == 1) && ((map2[a+1][b] == 0 || map2[a+1][b] == 2)&&(map2[a-1][b] == 0 || map2[a-1][b] == 2) && (map2[a][b-1] == 0 || map2[a][b-1] == 2) && (map2[a][b+1] == 0 || map2[a][b+1] == 2)))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"13.png"];//center
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if((map2[a][b] == 1) && ((map2[a+1][b] == 0 || map2[a+1][b] == 2)&&(map2[a-1][b] == 0 || map2[a-1][b] == 2) && (map2[a][b-1] == 0 || map2[a][b-1] == 2)))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"9.png"];//open right
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if((map2[a][b] == 1) && ((map2[a+1][b] == 0 || map2[a+1][b] == 2)&&(map2[a][b-1] == 0 || map2[a][b-1] == 2) && (map2[a][b+1] == 0 || map2[a][b+1] == 2)))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"10.png"];//open down
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if((map2[a][b] == 1) && ((map2[a+1][b] == 0 || map2[a+1][b] == 2)&&(map2[a-1][b] == 0 || map2[a-1][b] == 2) && (map2[a][b+1] == 0 || map2[a][b+1] == 2)))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"11.png"];//open left
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if((map2[a][b] == 1) && ((map2[a-1][b] == 0 || map2[a-1][b] == 2) && (map2[a][b-1] == 0 || map2[a][b-1] == 2) && (map2[a][b+1] == 0 || map2[a][b+1] == 2)))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"12.png"];//open up
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if((map2[a][b] == 1) && ((map2[a-1][b] == 0 || map2[a-1][b] == 2) && (map2[a][b-1] == 0 || map2[a][b-1] == 2)))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"5.png"];//open top right
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if((map2[a][b] == 1) && ((map2[a+1][b] == 0 || map2[a+1][b] == 2)&&(map2[a][b-1] == 0 || map2[a][b-1] == 2)))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"6.png"];//open right down
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if((map2[a][b] == 1) && ((map2[a+1][b] == 0 || map2[a+1][b] == 2) && (map2[a][b+1] == 0 || map2[a][b+1] == 2)))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"7.png"];//open down left
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if((map2[a][b] == 1) && ((map2[a-1][b] == 0 || map2[a-1][b] == 2) && (map2[a][b+1] == 0 || map2[a][b+1] == 2)))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"5.png"];//open top left
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if((map2[a][b] == 1) && ((map2[a][b-1] == 0 || map2[a][b-1] == 2) && (map2[a][b+1] == 0 || map2[a][b+1] == 2)))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"14.png"];//open up and down
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if((map2[a][b] == 1) && ((map2[a-1][b] == 0 || map2[a-1][b] == 2) && (map2[a+1][b] = 0 || map2[a+1][b] == 2)))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"15.png"];//open left and right
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if((map2[a][b] == 1) && (map2[a][b-1] == 0 || map2[a][b-1] == 2))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"1.png"];//closed left
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if((map2[a][b] == 1) && (map2[a+1][b] == 0 || map2[a+1][b] == 2))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"2.png"];//close top
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if((map2[a][b] == 1) && (map2[a][b+1] == 0 || map2[a][b+1] == 2))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"3.png"];//close right
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if((map2[a][b] == 1) && (map2[a-1][b] == 0 || map2[a-1][b] == 2))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"4.png"];//close down
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			//else if(b == 7 && sprite[a][b-1]==0)backSprites[a][b] = [CCSprite spriteWithFile:@"1.png"];
			
			
		}
	
	for(int yPosition = -12, a = 0, b = 0, c = 0; a<10;a++,b = 0, yPosition += 48)
		for(int xPosition = -30; b < 8; xPosition += 48, b++, c++)	
		{
			rects[a][b] = CGRectMake(xPosition,yPosition,48,48);
		}
	
	self.isTouchEnabled = true;
	
	return self;
}


-(void) ccTouchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
	UITouch* touch = [touches anyObject];
	CGPoint location = [touch locationInView: [touch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
		for(int a = 0; a < 8; a++)
		    for(int b = 0; b < 10; b++)
			{
				if(CGRectContainsPoint(rects[a][b], location) && map2[a][b] == 2)
				{
					[[CCDirector sharedDirector]replaceScene:[CCZoomFlipXTransition transitionWithDuration:1 scene: [classic node]]];
					sprite[a][b] = [CCSprite spriteWithFile:@"plain.png"];
					//currentX = b;
					//currentY = a;
				}
			}

		
}

-(void) ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	//map2[currentY][currentX] = 0;
}

@end
