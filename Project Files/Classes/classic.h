//
//  classic.h
//  Menu
//
//  Created by Joe Malott on 1/18/11.
//  Copyright 2011 0. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import <iAd/iAd.h>

#import <iAd/iAd.h>
#define AD_REFRESH_PERIOD 60.0 // once a  minute

@interface classic : CCLayer {
    
	CCSprite *backSprites[8][10];
	CCSprite *sprite[7][9];
    
    CCSprite *bg;
    
	NSTimer *refreshTimer;
	UIViewController *viewController;
	
}
+(id)scene;
-(bool)isDone;
-(void)randomSolvableMap;
-(void)drawMap;
-(void)doneWithLevel;
-(bool)noCaviat;
-(void)giveMapDefaultValue; 
@end
