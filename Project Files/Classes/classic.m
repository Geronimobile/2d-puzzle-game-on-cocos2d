//
//  classic.m
//  Menu
//
//  Created by Jonathan Malott on 1/18/11.
//  Copyright 2011 0. All rights reserved.
//

#import "help.h"
#import "HelloWorldScene.h"
#import "play.h"
#import "classic.h"
//#import "progressive.h"
#import "cocos2d.h"
#import "load.h"
//#import "iAd/ADInterstitialAd.h"

#import <iAd/iAd.h>

@implementation classic

//ADInterstitialAd interstitial;
int map2[10][8];
CGRect rects[10][8];
NSString *names[10][8] = {{@"1",@"2",@"3",@"4",@"5",@"6"},{@"7",@"8",@"9",@"10",@"11",@"12"},{@"13",@"14",@"15",@"16",@"17",@"18"},{@"19",@"20",@"21",@"22",@"23",@"24"},{@"25",@"26",@"27",@"28",@"29",@"30"},{@"31",@"32",@"33",@"34",@"35",@"36"},{@"37",@"38",@"39",@"40",@"41",@"42"},{@"43",@"44",@"45",@"46",@"47",@"48"}};
bool amIDone = false;
int currentX, currentY;
int startX[4], startY[4];
bool startExists[4];
bool onStart, doneWithThis = false;
int x,y,finalX,finalY;
int levelNumber;
bool onInitial;
bool hecksYes = false;
CCLabelTTF* label;

+ (void)initialize{
	onInitial = true;
	hecksYes = true;
	NSDictionary *appDefaults = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: [NSNumber numberWithInt:1], nil] forKeys:[NSArray arrayWithObjects:@"Level", nil]];
    [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
	
}

-(void) giveMapDefaultValue
{
	for(int a = 0; a < 10; a++)
		for(int b = 0; b < 8; b++)
		{
			if(hecksYes && !(a == 0 || a == 9 || b == 0 || b == 7)) [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:map2[a][b]] forKey:names[a-1][b-1]];
		}
	
	return;
}

+(id) scene
{
	CCScene *scene = [CCScene node];
	classic *layer = [classic node];
	[scene addChild: layer];
	return scene;
}

-(id) init
{
    
	levelNumber =[[NSUserDefaults standardUserDefaults] integerForKey:@"Level"];
	
	if(levelNumber == 1){
        [self randomSolvableMap];
        [self giveMapDefaultValue];
        onInitial = false;
    }
	
	for(int a = 0; a < 10; a++)
		for(int b = 0; b < 8; b++)
		{
			if(a == 0 || a == 9 || b == 0 || b == 7) map2[a][b] = -1;
			else map2[a][b] = [[NSUserDefaults standardUserDefaults] integerForKey:names[a-1][b-1]];
			if(map2[a][b] == 2) {finalX = b; finalY = a;}
			
		}
	
	
	if( (self=[super init] )) {
        
		CCSprite *background = [CCSprite spriteWithFile:@"blackbg.png"];
		background.tag = 1;
		background.anchorPoint = CGPointMake(0, 0);
		[self addChild:background];
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:12];
		
		CCMenuItemImage *item1 = [CCMenuItemImage itemFromNormalImage:@"reset.png" selectedImage:@"reset.png"
                                                               target:self
                                                             selector:@selector(doThis:)];
		
		item1.position = CGPointMake(320/2-50, 480/2-20);
		
		CCMenuItemImage *item2 = [CCMenuItemImage itemFromNormalImage:@"menu.png" selectedImage:@"menu.png"
															   target:self
															 selector:@selector(doThis2:)];
		
		item2.position = CGPointMake(320/2-280, 480/2-20);
		
		CCMenu *menu = [CCMenu menuWithItems:item1,item2, nil];
		[self addChild:menu];
        
		[self drawMap];
		
		label = [CCLabelTTF labelWithString:@"" fontName:@"Helvetica" fontSize:24];
		
        int offset;
        
        if([[[CCDirector sharedDirector] openGLView] window].frame.size.height == 480) offset = 480-20;
        else offset = 480+27;
        
        label.position =  ccp(320-160,offset);
		label.color = ccc3(255,255,255);
		[self addChild: label];
		[label setString:[NSString stringWithFormat:@"%d",levelNumber]];
		
		self.isTouchEnabled = true;
		
	}
    
	return self;
	
}

-(void)randomSolvableMap
{
	doneWithThis = false;
	while(true)
	{
		while(true){
			for(int i = 0; i < 4; i++) startExists[i] = false;
			for(int a = 0; a < 10; a++)
				for(int b = 0; b < 8; b++)//make map
				{
					int rand = arc4random()%21;
					if(a == 0 || a == 9 || b == 0 || b == 7) map2[a][b] = -1;
					else if(rand <= 15) map2[a][b] = 0;
					else map2[a][b] = 1;
				}
			if([self noCaviat]) break;
		}
		
		while(true){
			x = (arc4random()%6)+1;
			y = (arc4random()%7)+1;
			finalX = x;
			finalY = y;
			if(map2[y][x] == 0) {
				map2[y][x] = 2;
				break;
			}
		}
		
		for(int i = 0; i < 350; i++){
			//if([self isDone]) break;
			for(int a = 0; a < 10; a++){
				for(int b = 0; b < 8; b++)
					if(map2[a][b] == 5) map2[a][b] = 0;}//clear map to original state
			x = finalX;
			y = finalY;
			while(true){
				int direction = arc4random()%4;
				if(map2[y][x+1] != 0 && map2[y][x-1] != 0 && map2[y-1][x] != 0 && map2[y+1][x] != 0)break;
				if(direction == 0 && map2[y][x+1] == 0){
					x++;
					map2[y][x] = 5;
				}
				else if(direction == 1 && map2[y][x-1] == 0){
					x--;
					map2[y][x] = 5;
				}
				else if(direction == 2 && map2[y+1][x] == 0){
					y++;
					map2[y][x] = 5;
				}
				else if(direction == 3 && map2[y-1][x] == 0){
					y--;
					map2[y][x] = 5;
				}}
			if([self isDone]) break;
		}
		if([self isDone]) break;
	}
	
	
	for(int a = 1; a < 9; a++)
		for(int b = 1; b < 7; b++)
			if(map2[a][b] != 5 && map2[a][b] != 2) map2[a][b] = 1;
	
	for(int a = 0; a < 10; a++)
		for(int b = 0; b < 8; b++)
		    if(map2[a][b] == 5) map2[a][b] = 0;
	
	return;
	
	
}

-(bool) isDone
{
	for(int a = 0; a < 8; a++)
		for(int b = 0; b < 11; b++)
			if(map2[a][b] == 0)return false;
	return true;
}

-(void) drawMap
{
    
    bg = [CCSprite spriteWithFile:[NSString stringWithFormat:@"bg%d.png",levelNumber%11+1]];
    
    int offset;
    
    if([[[CCDirector sharedDirector] openGLView] window].frame.size.height == 480) offset = 0;
    else offset = 48;
    
    bg.position = ccp(164,232 + offset);
    [self addChild:bg ];
	
    for(int yPosition = 12 + offset, a = 0, b = 0; a<9;a++,b = 0, yPosition += 48)
		for(int xPosition = -6; b < 7; xPosition += 48, b++)
        {
            if((map2[a][b] == 0 || map2[a][b] == 2) )
			{
				backSprites[a][b] = [CCSprite spriteWithFile:@"master.png"];
				backSprites[a][b].position = ccp(xPosition,yPosition);
				[self addChild:backSprites[a][b]];
			}
        }
    
    for(int yPosition = 12 + offset, a = 0, b = 0, c = 0; a<10;a++, yPosition += 48,b = 0)
		for(int xPosition = -6; b < 8; xPosition += 48, b++, c++)
		{
            
			if(map2[a][b] == 0)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"plain.png"];
				sprite[a][b].position = CGPointMake(xPosition,yPosition);
				[self addChild:sprite[a][b]];
			}
			else if(map2[a][b] == 2)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"start.png"];
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];
			}
			else if(map2[a][b] == 5)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"done.png"];
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];
			}
            
		}
    
	for(int yPosition = -12 + offset, a = 0, b = 0; a<10 ;a++,b = 0, yPosition += 48)
		for(int xPosition = -30; b < 8; xPosition += 48, b++)
		{
			rects[a][b] = CGRectMake(xPosition,yPosition,48,48);
		}
    
	return;
}

-(bool)noCaviat
{
	
	for(int a = 1; a < 9; a++)
		for(int b = 1; b < 7; b++)
			if(map2[a][b] == 0 && (map2[a][b+1] == 1 || map2[a][b+1] == -1) && (map2[a][b-1] == 1 || map2[a][b-1] == -1) && (map2[a+1][b] == 1 || map2[a+1][b] == -1) && (map2[a-1][b] == 1 || map2[a-1][b] == -1)) return false;
	
	return true;
}

-(bool)deadEnd{
    
    if([self isDone])return false
        ;
    
    if((map2[currentY][currentX-1] == 0)||
       (map2[currentY][currentX+1] == 0)||
       (map2[currentY-1][currentX] == 0)||
       (map2[currentY+1][currentX] == 0)){
        
        return false;
        
    }
    
    UIAlertView *final = [[UIAlertView alloc] init];
    [final setDelegate:self];
    [final setTitle:@"There are no more moves."];
    [final setMessage:@"Please click below to…"];
    [final addButtonWithTitle:@"Repeat Level"];
    [final addButtonWithTitle:@"Restart Game"];
    [final show];
    [final release];
    
    return true;
    
}

-(void)ccTouchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    
	UITouch* touch = [touches anyObject];
	CGPoint location = [touch locationInView: [touch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
    
	for(int a = 0; a < 10 ;a++)
		for(int b = 0; b < 8;b++)
			if(CGRectContainsPoint(rects[a][b], location) && map2[a][b] == 2)
			{
				currentX = b;
				currentY = a;
				onStart = true;
			}
	
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch* touch = [touches anyObject];
	CGPoint location = [touch locationInView: [touch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
    
    if((CGRectContainsPoint(rects[currentY][currentX-1], location) && onStart && (map2[currentY][currentX-1] == 0))||
       (CGRectContainsPoint(rects[currentY][currentX+1], location) && onStart && (map2[currentY][currentX+1] == 0))||
       (CGRectContainsPoint(rects[currentY-1][currentX], location) && onStart && (map2[currentY-1][currentX] == 0))||
       (CGRectContainsPoint(rects[currentY+1][currentX], location) && onStart && (map2[currentY+1][currentX] == 0))){
        
        [sprite[currentY][currentX] setTexture:[[CCTextureCache sharedTextureCache] addImage:@"blank.png"]];
        
        [backSprites[currentY][currentX] setTexture:[[CCTextureCache sharedTextureCache] addImage:@"blank.png"]];
        
        map2[currentY][currentX] = 3;
        
    } else return;
    
	if(CGRectContainsPoint(rects[currentY][currentX-1], location) && onStart && (map2[currentY][currentX-1] == 2 || map2[currentY][currentX-1] == 0))currentX--;
	else if(CGRectContainsPoint(rects[currentY][currentX+1], location) && onStart && (map2[currentY][currentX+1] == 2 || map2[currentY][currentX+1] == 0))currentX++;
    else if(CGRectContainsPoint(rects[currentY-1][currentX], location) && onStart && (map2[currentY-1][currentX] == 2 || map2[currentY-1][currentX] == 0))currentY--;
	else if(CGRectContainsPoint(rects[currentY+1][currentX], location) && onStart && (map2[currentY+1][currentX] == 2 || map2[currentY+1][currentX] == 0))currentY++;
    
    map2[currentY][currentX] = 2;
    
    [sprite[currentY][currentX] setTexture:[[CCTextureCache sharedTextureCache] addImage:@"start.png"]];
    
    [self deadEnd];
    
	if([self isDone] ) doneWithThis = true;
    
}

-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	if(map2[currentY+1][currentX] != -1) onStart = false;
	
	if(doneWithThis)[self doneWithLevel];
}

-(void)doneWithLevel
{
	if(doneWithThis){
        
        
    if(levelNumber % 5 == 0){
        
        // Send data game -> server
        NSString *body = @"level=5&device=ios";
        NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://geronimobile.com/server/glofish/in.php"]];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[body dataUsingEncoding:NSStringEncodingConversionExternalRepresentation]];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        NSError *err = nil;
        
        if (err != nil) { NSLog(@"Failed"); }
       // NSString* answer = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        }
        
		[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:levelNumber+1] forKey:@"Level"];
        
		[self randomSolvableMap];
		hecksYes = true;
		[self giveMapDefaultValue];
		[[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:1.5f scene: [classic node]]];
		
	}
	
}

-(void)doThis:(id)sender{
	
	for(int a = 0; a < 10; a++)
		for(int b = 0; b < 8; b++)
			if(map2[a][b] == 3 || map2[a][b] == 2) map2[a][b] = 0;
	
	map2[finalY][finalX] = 2;
	
    for(int a = 0, b = 0; a<10;a++,b = 0)
		for(; b < 8;b++)
            if(map2[a][b] == 0 || map2[a][b] == 2)[backSprites[a][b] setTexture:[[CCTextureCache sharedTextureCache] addImage:@"master.png"]];
	
    for(int yPosition = 12, a = 0, b = 0, c = 0; a<10;a++, yPosition += 48,b = 0)
		for(int xPosition = -6; b < 8; xPosition += 48, b++, c++)
		{
			if(map2[a][b] == 0)[sprite[a][b] setTexture:[[CCTextureCache sharedTextureCache] addImage:@"plain.png"]];
			else if(map2[a][b] == 2)[sprite[a][b]setTexture:[[CCTextureCache sharedTextureCache] addImage:@"start.png"]];
			//else if(map2[a][b] == 1)[backSprites[a][b]setTexture:[[CCTextureCache sharedTextureCache] addImage:@"black.png"]];
			else if(map2[a][b] == 5)[sprite[a][b] setTexture:[[CCTextureCache sharedTextureCache] addImage:@"done.png"]];
            
        }
    
    
}

-(void) alertView:(UIAlertView *)alert clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alert.tag == 2){
        if(buttonIndex == 1){
            [self doThis:self];
        }else {
            [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:1] forKey:@"Level"];
            [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:1 scene: [classic node]]];
        }
    } else {
        if(buttonIndex == 1){
            
            UIAlertView *final = [[UIAlertView alloc] init];
            final.tag = 2;
            [final setDelegate:self];
            [final setTitle:@""];
            [final setMessage:@"Are you sure that you would like to start over from Level 1?"];
            [final addButtonWithTitle:@"Yes"];
            [final addButtonWithTitle:@"Cancel"];
            [final show];
            [final release];
            
        } else
            [self doThis:self];
    }
    
    
}

-(void)doThis2:(id)sender
{
	[[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:1 scene: [HelloWorld node]]];
}

@end
