//
//  credits.m
//  Menu
//
//  Created by Joe Malott on 2/20/11.
//  Copyright 2011 0. All rights reserved.
//

#import "help.h"
#import "HelloWorldScene.h"
#import "play.h"
#import "classic.h"
#import "progressive.h"
#import "gmaeOptions.h"
#import "soundOptions.h"
#import "credits.h"

@implementation credits
+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	//userInteractionEnables = true;
	
	// 'layer' is an autorelease object.
	credits *layer = [credits node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init
{
	if( (self=[super init] )) {
		CCSprite* background = [CCSprite spriteWithFile:@"main.png"];
		background.tag = 1;
		background.anchorPoint = CGPointMake(0, 0);
		[self addChild:background];
	}
	/*CCLabel* label1 = [CCLabel labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:30];
	label1.position =  ccp(320/2,480/2+80);
	label1.color = ccc3(0,0,0);
	[self addChild: label1];
	CCLabel* label2 = [CCLabel labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:30];
	label2.position =  ccp(320/2,480/2+40);
	label2.color = ccc3(0,0,0);
	[self addChild: label2];
	CCLabel* label3 = [CCLabel labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:40];
	label3.position =  ccp(320/2,480/2+00);
	label3.color = ccc3(0,0,0);
	[self addChild: label3];
	CCLabel* label4 = [CCLabel labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:30];
	label4.position =  ccp(320/2,480/2-40);
	label4.color = ccc3(0,0,0);
	[self addChild: label4];
	CCLabel* label5 = [CCLabel labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:40];
	label5.position =  ccp(320/2,480/2-80);
	label5.color = ccc3(0,0,0);
	[self addChild: label5];
	CCLabel* label6 = [CCLabel labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:30];
	label6.position =  ccp(320/2,480/2-120);
	label6.color = ccc3(0,0,0);
	[self addChild: label6];
	CCLabel* label7 = [CCLabel labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:30];
	label7.position =  ccp(320/2,480/2-160);
	label7.color = ccc3(0,0,0);
	[self addChild: label7];
	CCLabel* label8 = [CCLabel labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:25];
	label8.position =  ccp(320/2,480/2-190);
	label8.color = ccc3(0,0,0);
	[self addChild: label8];
	
	
	[label1 setString:@"© Malott Computer"];
	[label2 setString:@"Science Company"];
	[label3 setString:@"Senior Developer"];
	[label4 setString:@"Jonathan Malott"];
	[label5 setString:@"Graphics"];
	[label6 setString:@"Kent Nguyen"];
	[label7 setString:@"Special thanks:"];
	[label8 setString:@"Chelsea Chanay and David Buksa"];
	
	
	
	
	CCMenuItemImage *itema = [CCMenuItemImage itemFromNormalImage:@"backOff.png" selectedImage:@"backOn.png"
														   target:self
														 selector:@selector(doThis2:)];
	
	itema.position = ccp(0, -480/2+25+0);
	
	CCMenu *menua = [CCMenu menuWithItems:itema, nil];
	[self addChild:menua];	*/
	
	return self;
}

-(void)doThis2:(id)sender{
	
	[[CCDirector sharedDirector]replaceScene:[CCTransitionFlipX transitionWithDuration:1 scene: [HelloWorld node]]];
}

@end
