//
//  gmaeOptions.m
//  Menu
//
//  Created by Joe Malott on 2/20/11.
//  Copyright 2011 0. All rights reserved.
//

#import "help.h"
#import "HelloWorldScene.h"
#import "play.h"
#import "classic.h"
#import "progressive.h"
#import "gmaeOptions.h"
#import "soundOptions.h"
#import "credits.h"


@implementation gmaeOptions

int levelNumber, levelNumber2, levelNumber3;

+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	//userInteractionEnables = true;
	
	// 'layer' is an autorelease object.
	gmaeOptions *layer = [gmaeOptions node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}
-(id) init
{
	if( (self=[super init] )) {
        int number = arc4random()%3;
        
        if(number == 0){
            
            CCSprite* background = [CCSprite spriteWithFile:@"Background2.png"];
            background.tag = 1;
            background.anchorPoint = CGPointMake(0, 0);
            [self addChild:background];
            
            CCSprite* background2 = [CCSprite spriteWithFile:@"Backgroundstripes2.png"];
            background2.tag = 2;
            background2.anchorPoint = CGPointMake(0, 0);
            [self addChild:background2];
            
        } else if(number == 1){
            
            CCSprite* background = [CCSprite spriteWithFile:@"Background3.png"];
            background.tag = 1;
            background.anchorPoint = CGPointMake(0, 0);
            [self addChild:background];
            
            CCSprite* background2 = [CCSprite spriteWithFile:@"Backgroundstripes3.png"];
            background2.tag = 2;
            background2.anchorPoint = CGPointMake(0, 0);
            [self addChild:background2];
            
        } else {
            
            CCSprite* background = [CCSprite spriteWithFile:@"Background5.png"];
            background.tag = 1;
            background.anchorPoint = CGPointMake(0, 0);
            [self addChild:background];
            
            CCSprite* background2 = [CCSprite spriteWithFile:@"Backgroundstripes5.png"];
            background2.tag = 2;
            background2.anchorPoint = CGPointMake(0, 0);
            [self addChild:background2];
            
        }
        
        CCSprite* logo = [CCSprite spriteWithFile:@"mainlogoSMALL.png"];
        logo.tag = 3;
        logo.position = CGPointMake(170, 400);
        [self addChild:logo];
	}
		
	CCLabelTTF* label4 = [CCLabelTTF labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:50];
	label4.position =  ccp(320/2,480/2+80);
	label4.color = ccc3(0,0,0);
	[self addChild: label4];
	[label4 setString:@"Game Options"];
	CCLabelTTF* label5 = [CCLabelTTF labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:32];
	label5.position =  ccp(320/2-55,480/2-10+30);
	label5.color = ccc3(0,0,0);
	[self addChild: label5];
	[label5 setString:@"Classic mode"];
	
	levelNumber =[[NSUserDefaults standardUserDefaults]
				  integerForKey:@"Level"];
	
	CCLabelTTF* label = [CCLabelTTF labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:28];
	label.position =  CGPointMake(320/2-28-10,480/2-35+30);
	label.color = ccc3(0,0,0);
	[self addChild: label];
	[label setString:[NSString stringWithFormat:@"current level: %d",levelNumber]];
	
	CCMenuItemImage *item1 = [CCMenuItemImage itemFromNormalImage:@"reset.png" selectedImage:@"reset.png"
														   target:self
														 selector:@selector(item1Action:)];
	CCMenu *menu = [CCMenu menuWithItems:item1, nil];
	item1.position = ccp(98,-35+30);
	[self addChild:menu];
	
    
	CCMenuItemImage *itema = [CCMenuItemImage itemFromNormalImage:@"backOff.png" selectedImage:@"backOn.png"
														   target:self
														 selector:@selector(doThis2:)];
	
	itema.position = ccp(0, -480/2+25+30);

	CCMenu *menua = [CCMenu menuWithItems:itema, nil];
	[self addChild:menua];	
	//self.isTouchEnabled = YES;
	
	
	CCLabelTTF* label6 = [CCLabelTTF labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:32];
	label6.position =  ccp(320/2-26,480/2-70+30);
	label6.color = ccc3(0,0,0);
	[self addChild: label6];
	[label6 setString:@"Progressive mode"];
	
	levelNumber2 =[[NSUserDefaults standardUserDefaults]
				  integerForKey:@"CurrentLevel"];
	
	CCLabelTTF* label7 = [CCLabelTTF labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:28];
	label7.position =  CGPointMake(320/2-26-20,480/2-99+30);
	label7.color = ccc3(0,0,0);
	[self addChild: label7];
	[label7 setString:[NSString stringWithFormat:@"current level: %d",levelNumber2]];
	
	
	CCMenuItemImage *item2 = [CCMenuItemImage itemFromNormalImage:@"reset.png" selectedImage:@"reset.png"
														   target:self
														 selector:@selector(item2Action:)];
	CCMenu *menu3 = [CCMenu menuWithItems:item2, nil];
	item2.position = ccp(96,-98+30);
	[self addChild:menu3];
	
	levelNumber3 =[[NSUserDefaults standardUserDefaults]
				   integerForKey:@"HighestLevel"];
	
	CCLabelTTF* label8 = [CCLabelTTF labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:28];
	label8.position =  CGPointMake(320/2-26-20,480/2-129+30);
	label8.color = ccc3(0,0,0);
	[self addChild: label8];
	[label8 setString:[NSString stringWithFormat:@"highest level: %d",levelNumber3]];
	
	
	CCMenuItemImage *item3 = [CCMenuItemImage itemFromNormalImage:@"reset.png" selectedImage:@"reset.png"
														   target:self
														 selector:@selector(item3Action:)];
	CCMenu *menu4 = [CCMenu menuWithItems:item3, nil];
	item3.position = ccp(96,-130+30);
	[self addChild:menu4];
	
	return self;
}

-(void)item1Action:(id)sender{
	[[NSUserDefaults standardUserDefaults] setObject:[NSNumber
													  numberWithInt:1] forKey:@"Level"];
	[[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.01f scene: [gmaeOptions node]]];
}

	 -(void)item2Action:(id)sender{
		 [[NSUserDefaults standardUserDefaults] setObject:[NSNumber
														   numberWithInt:1] forKey:@"CurrentLevel"];
		 [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.01f scene: [gmaeOptions node]]];
	 }
	 
	 -(void)item3Action:(id)sender{
		 [[NSUserDefaults standardUserDefaults] setObject:[NSNumber
														   numberWithInt:1] forKey:@"HighestLevel"];
		 [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.01f scene: [gmaeOptions node]]];
	 }

-(void)doThis2:(id)sender{

	[[CCDirector sharedDirector]replaceScene:[CCTransitionFlipX transitionWithDuration:1 scene: [help node]]];
}

@end
