//JM The King Of Code

#import "help.h"
#import "HelloWorldScene.h"
#import "play.h"
#import "classic.h"
#import "progressive.h"
#import "gmaeOptions.h"
#import "soundOptions.h"
#import "credits.h"

@implementation help
+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	help *layer = [help node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init
{
	if( (self=[super init] )) {
        int number = arc4random()%3;
        
        if(number == 0){
            
            CCSprite* background = [CCSprite spriteWithFile:@"Background2.png"];
            background.tag = 1;
            background.anchorPoint = CGPointMake(0, 0);
            [self addChild:background];
            
            CCSprite* background2 = [CCSprite spriteWithFile:@"Backgroundstripes2.png"];
            background2.tag = 2;
            background2.anchorPoint = CGPointMake(0, 0);
            [self addChild:background2];
            
        } else if(number == 1){
            
            CCSprite* background = [CCSprite spriteWithFile:@"Background3.png"];
            background.tag = 1;
            background.anchorPoint = CGPointMake(0, 0);
            [self addChild:background];
            
            CCSprite* background2 = [CCSprite spriteWithFile:@"Backgroundstripes3.png"];
            background2.tag = 2;
            background2.anchorPoint = CGPointMake(0, 0);
            [self addChild:background2];
            
        } else {
            
            CCSprite* background = [CCSprite spriteWithFile:@"Background5.png"];
            background.tag = 1;
            background.anchorPoint = CGPointMake(0, 0);
            [self addChild:background];
            
            CCSprite* background2 = [CCSprite spriteWithFile:@"Backgroundstripes5.png"];
            background2.tag = 2;
            background2.anchorPoint = CGPointMake(0, 0);
            [self addChild:background2];
            
        }
        
        CCSprite* logo = [CCSprite spriteWithFile:@"mainlogoSMALL.png"];
        logo.tag = 3;
        logo.position = CGPointMake(170, 400);
        [self addChild:logo];
		
		
		
		CCMenuItemImage *item1 = [CCMenuItemImage itemFromNormalImage:@"newOff.png" selectedImage:@"newOn.png"
															   target:self
															 selector:@selector(item1Action:)];
	
		
		CCMenuItemImage *item3 = [CCMenuItemImage itemFromNormalImage:@"creditsOff.png" selectedImage:@"creditsOn.png"
															   target:self
															 selector:@selector(item3Action:)];
		
		CCMenuItemImage *item4 = [CCMenuItemImage itemFromNormalImage:@"backOff.png" selectedImage:@"backOn.png"
															   target:self
															 selector:@selector(item4Action:)];
		
		
		
		CCMenu *menu = [CCMenu menuWithItems:item1,item3,item4, nil];
		item1.position = ccp(0, 90-50);
		item3.position = ccp(0, -20-50);
		item4.position = ccp(0, -120-50);
		
		[self addChild:menu];
		
		
		//[self addChild:menu];
	}
	


	
	return self;
}
-(void)doThis:(id)sender{
	[[CCDirector sharedDirector]replaceScene:[CCTransitionFlipX transitionWithDuration:1 scene: [HelloWorld node]]];
}

-(void)item1Action:(id)sender{
	[[CCDirector sharedDirector]replaceScene:[CCTransitionFlipX transitionWithDuration:1 scene: [gmaeOptions node]]];
}

-(void)item2Action:(id)sender{
	[[CCDirector sharedDirector]replaceScene:[CCTransitionFlipX transitionWithDuration:1 scene: [soundOptions node]]];
}

-(void)item3Action:(id)sender{
	//[[CCDirector sharedDirector]replaceScene:[CCZoomFlipXTransition transitionWithDuration:1 scene: [credits node]]];
    
    
    
    UIAlertView* dialog = [[UIAlertView alloc] init];
	[dialog setDelegate:self];
	[dialog setTitle:@"Credits"];
	[dialog setMessage:@"Created by Jonathan Malott.  Copyright © 2012 Geronimobile Studios.  Thanks to everyone who helped make Rub Labyrinth a success!  Special thanks to Chelsea Chanay, David Buksa, and Kent Nguyen.  Created with Cocos2D game engine.  Final thanks to my AMAZING girlfriend Leann!"];
	[dialog addButtonWithTitle:@"awesome."];
	[dialog show];
	[dialog release];
    
    
    
    
}

-(void)item4Action:(id)sender{
	[[CCDirector sharedDirector]replaceScene:[CCTransitionFlipX transitionWithDuration:1 scene: [HelloWorld node]]];
}


- (void) alertView:(UIAlertView *)alert clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if(buttonIndex==0) {
        //do stuff
    }
}

@end
