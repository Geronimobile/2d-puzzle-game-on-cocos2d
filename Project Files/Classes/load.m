//
//  load.m
//  Menu
//
//  Created by Joe Malott on 2/5/11.
//  Copyright 2011 0. All rights reserved.
//

#import "load.h"
#import "HelloWorldScene.h"
#import "classic.h"


@implementation load

+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	load *layer = [load node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}


-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init] )) {
		
		CCSprite* background = [CCSprite spriteWithFile:@"main3.png"];
		background.tag = 1;
		background.anchorPoint = CGPointMake(0, 0);
		[self addChild:background];
		
		[[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:1 scene: [classic node]]];

		CCLabelTTF *lbl01 = [CCLabelTTF labelWithString:@"loading..." dimensions:CGSizeMake(300, 60) alignment:UITextAlignmentLeft fontName:@"Helvetica" fontSize:16];
		[lbl01 setPosition: CGPointMake(160,330)];
		[self addChild: lbl01];
	}
	
	[[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:1 scene: [classic node]]];
	
	return self;
	
	
	
	
}


@end
