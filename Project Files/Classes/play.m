//
//  play.m
//  Menu
//
//  Created by Joe Malott on 1/18/11.
//  Copyright 2011 0. All rights reserved.
//

#import "help.h"
#import "HelloWorldScene.h"
#import "play.h"
#import "classic.h"
//#import "progressive.h"
#import "load.h"

@implementation play
+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	play *layer = [play node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init
{
	if( (self=[super init] )) {
		
        
        
        int number = arc4random()%3;
        
        if(number == 0){
            
            CCSprite* background = [CCSprite spriteWithFile:@"Background2.png"];
            background.tag = 1;
            background.anchorPoint = CGPointMake(0, 0);
            [self addChild:background];
            
            CCSprite* background2 = [CCSprite spriteWithFile:@"Backgroundstripes2.png"];
            background2.tag = 2;
            background2.anchorPoint = CGPointMake(0, 0);
            [self addChild:background2];
            
        } else if(number == 1){
            
            CCSprite* background = [CCSprite spriteWithFile:@"Background3.png"];
            background.tag = 1;
            background.anchorPoint = CGPointMake(0, 0);
            [self addChild:background];
            
            CCSprite* background2 = [CCSprite spriteWithFile:@"Backgroundstripes3.png"];
            background2.tag = 2;
            background2.anchorPoint = CGPointMake(0, 0);
            [self addChild:background2];
            
        } else {
            
            CCSprite* background = [CCSprite spriteWithFile:@"Background5.png"];
            background.tag = 1;
            background.anchorPoint = CGPointMake(0, 0);
            [self addChild:background];
            
            CCSprite* background2 = [CCSprite spriteWithFile:@"Backgroundstripes5.png"];
            background2.tag = 2;
            background2.anchorPoint = CGPointMake(0, 0);
            [self addChild:background2];
            
        }
        
        CCSprite* logo = [CCSprite spriteWithFile:@"mainlogoSMALL.png"];
        logo.tag = 3;
        logo.position = CGPointMake(170, 400);
        [self addChild:logo];
        
        
		CCMenuItemImage *item1 = [CCMenuItemImage itemFromNormalImage:@"classicoff.png" selectedImage:@"classicon.png"
															   target:self
															 selector:@selector(item1Action:)];
		CCMenuItemImage *item2 = [CCMenuItemImage itemFromNormalImage:@"progressiveoff.png" selectedImage:@"progressiveon.png"
															   target:self
															 selector:@selector(item2Action:)];
		
		
		item1.position = ccp(0, -25);
		item2.position = ccp(0, -90);
		//[menu alignItemsVertically];
		
		
		
		CCMenuItemImage *item4 = [CCMenuItemImage itemFromNormalImage:@"backOff.png" selectedImage:@"backOn.png"
															   target:self
															 selector:@selector(item4Action:)];
		
		
		
		CCMenu *menu = [CCMenu menuWithItems:item1,item2,item4, nil];
		item4.position = ccp(0, -120-50);
		
		[self addChild:menu];
        
        
        
        
        
        
        CCLabelTTF *label = [CCLabelTTF labelWithString:@"Select Game Type" fontName:@"Marker Felt" fontSize:35];
		label.position =  ccp(320-160,480-180);
		label.color = ccc3(0,0,0);
		[self addChild: label];
        
        
        
		
	    }
	return self;
}
-(void)item1Action:(id)sender{	
	[[CCDirector sharedDirector]replaceScene:[CATransition transitionWithDuration:1 scene: [classic node]]];
}

-(void)item2Action:(id)sender{
	//[[CCDirector sharedDirector]replaceScene:[CCFadeTransition transitionWithDuration:1 scene: [progressive node]]];
}

-(void)item4Action:(id)sender{
	[[CCDirector sharedDirector]replaceScene:[CCTransitionFlipX transitionWithDuration:1 scene: [HelloWorld node]]];
}

@end
