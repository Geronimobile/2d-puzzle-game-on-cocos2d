//
//  classic.h
//  Menu
//
//  Created by Joe Malott on 1/18/11.
//  Copyright 2011 0. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface progressive : CCLayer {
	CCSprite *sprite[6][8];
	CCSprite *backSprites[8][10];
	bool hecksYes;
	
}
+(id)scene;
-(bool)isDone;
-(void)randomSolvableMap;
-(void)drawMap;
-(bool)noCaviat;
-(void)doThis2;
-(void)giveMapDefaultValue; 
@end
