//
//  classic.m
//  Menu
//
//  Created by Joe Malott on 1/18/11.
//  Copyright 2011 0. All rights reserved.
//

#import "help.h"
#import "HelloWorldScene.h"
#import "play.h"
#import "classic.h"
#import "progressive.h"
#import "cocos2d.h"
#import "load.h"
#import "SettingsManager.h"

@implementation progressive

int thisMap[10][8];
CGRect rects[10][8];
bool doneWithThis, onStart;
int currentX, currentY, finalX, finalY, endX, endY, levelNumber;
int x,y,finalX,finalY, currentLevel, highestLevel, lives;
NSString *names[10][8];
CCLabel* label3;
int iterations = 0;
bool onInitial = false;



int map1[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,2,1,1,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,1,0,0,1,0,0,-1},
	{-1,1,0,0,1,0,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,0,0,1,0,0,0,-1},
	{-1,1,4,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map_2[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
				   {-1,1,1,2,1,1,1,-1},
			       {-1,1,0,0,1,1,1,-1},
				   {-1,0,0,0,0,0,0,-1},
				   {-1,0,0,0,0,0,0,-1},
				   {-1,0,0,1,1,0,0,-1},
				   {-1,0,1,0,0,0,0,-1},
				   {-1,0,0,0,1,1,0,-1},
				   {-1,1,1,1,1,1,4,-1},
				   {-1,-1,-1,-1,-1,-1,-1,-1}};

int map3[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
				   {-1,1,1,1,1,1,2,-1},
				   {-1,0,0,0,1,0,0,-1},
				   {-1,0,0,0,0,0,0,-1},
				   {-1,0,0,1,0,0,0,-1},
				   {-1,0,1,0,0,1,1,-1},
				   {-1,0,0,0,0,0,0,-1},
				   {-1,1,0,0,0,0,0,-1},
				   {-1,1,4,1,1,1,1,-1},
				   {-1,-1,-1,-1,-1,-1,-1,-1}};

int map4[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
				   {-1,1,1,2,1,1,1,-1},
				   {-1,1,1,0,0,1,1,-1},
				   {-1,1,1,1,0,0,1,-1},
				   {-1,1,0,0,1,0,0,-1},
				   {-1,0,0,0,0,0,0,-1},
				   {-1,0,0,0,1,1,1,-1},
				   {-1,0,0,0,0,0,0,-1},
				   {-1,1,1,1,1,1,4,-1},
				   {-1,-1,-1,-1,-1,-1,-1,-1}};

int map5[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,1,2,-1},
	{-1,1,0,0,1,0,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,0,0,1,0,0,0,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,1,1,1,1,4,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map6[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,2,1,1,1,1,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map7[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,1,2,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,0,0,0,1,0,1,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,1,4,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map8[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,2,1,1,-1},
	{-1,1,1,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,1,1,1,4,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map9[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,2,1,1,1,1,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,1,0,0,0,0,-1},
	{-1,0,1,0,1,1,0,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,0,0,1,0,0,0,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map10[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,1,2,-1},
	{-1,0,0,1,1,1,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,0,1,0,0,0,0,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map11[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,2,1,1,1,1,-1},
	{-1,1,0,1,0,0,1,-1},
	{-1,0,0,1,0,0,0,-1},
	{-1,0,1,0,0,1,0,-1},
	{-1,0,1,0,0,0,0,-1},
	{-1,0,1,1,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,1,1,1,1,4,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map12[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,1,2,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,1,0,0,0,1,-1},
	{-1,0,1,0,0,0,0,-1},
	{-1,0,0,1,0,1,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,1,0,0,0,0,1,-1},
	{-1,1,4,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map13[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,2,1,1,1,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,1,0,0,1,1,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,1,1,1,1,1,4,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map14[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,1,2,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,1,0,0,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,1,0,0,0,0,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map15[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,2,1,1,-1},
	{-1,0,0,1,0,0,1,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,1,1,1,1,4,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map16[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,2,1,1,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,1,0,0,0,0,-1},
	{-1,0,1,0,0,0,0,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,1,1,1,1,1,4,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map17[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,1,2,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,1,1,0,0,0,1,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,1,1,1,1,1,4,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map18[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,2,1,1,1,1,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,1,0,0,0,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,1,0,1,0,0,0,-1},
	{-1,1,4,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map19[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,2,1,1,1,1,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,1,1,0,-1},
	{-1,1,1,1,0,0,0,-1},
	{-1,0,0,0,0,1,1,-1},
	{-1,0,1,1,0,0,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,1,1,1,1,1,4,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map20[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,2,1,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map21[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,2,1,-1},
	{-1,0,0,1,1,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,1,0,0,0,1,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,0,1,1,0,0,0,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map22[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,1,2,-1},
	{-1,1,1,1,1,1,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,0,0,1,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map23[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,1,2,-1},
	{-1,1,0,0,1,1,0,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,0,1,0,1,0,0,-1},
	{-1,0,0,0,1,1,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,0,0,1,0,0,0,-1},
	{-1,1,4,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map24[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,2,1,1,1,1,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,1,1,0,0,0,0,-1},
	{-1,1,0,0,1,0,0,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map25[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,2,1,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,0,1,1,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,1,1,1,1,1,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map26[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,2,1,1,1,1,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,1,0,0,0,0,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,0,0,1,1,0,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,0,0,1,1,0,0,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map27[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,2,1,1,1,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,0,0,1,0,1,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,1,0,0,1,0,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,1,0,0,1,1,0,-1},
	{-1,1,1,1,1,1,4,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map28[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,2,1,1,1,1,1,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,1,0,1,0,1,0,-1},
	{-1,1,0,1,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,1,1,1,4,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map29[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,1,2,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,1,0,0,0,0,-1},
	{-1,0,0,1,0,0,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map30[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,2,1,1,1,1,1,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,1,1,0,0,0,0,-1},
	{-1,1,1,0,0,1,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,1,1,1,1,1,4,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map31[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,2,1,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,0,1,1,0,0,1,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,1,0,0,0,-1},
	{-1,0,0,1,1,1,0,-1},
	{-1,1,1,1,1,1,4,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map32[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,2,1,1,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,1,0,0,1,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map33[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,2,1,1,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,1,1,0,-1},
	{-1,0,0,1,1,0,0,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map34[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,1,2,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,1,0,0,0,0,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map35[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,2,1,-1},
	{-1,1,0,0,1,0,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,1,0,0,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,1,1,1,1,1,4,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map36[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,2,1,1,1,1,-1},
	{-1,0,0,1,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,0,0,0,1,1,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,0,0,0,0,1,1,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map37[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,2,1,1,1,1,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,0,0,0,1,1,0,-1},
	{-1,0,1,0,0,0,0,-1},
	{-1,0,1,1,0,0,0,-1},
	{-1,0,0,1,0,0,0,-1},
	{-1,1,4,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map38[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,2,1,1,1,1,1,-1},
	{-1,0,0,1,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,1,0,0,0,1,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,1,1,1,4,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map39[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,1,2,-1},
	{-1,1,1,1,1,0,0,-1},
	{-1,1,0,0,1,0,1,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,0,0,0,1,1,0,-1},
	{-1,1,0,0,0,1,0,-1},
	{-1,0,0,1,0,0,0,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map40[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,2,1,1,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,0,1,0,0,0,0,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map41[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,2,1,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,0,1,0,0,0,1,-1},
	{-1,0,1,0,0,0,0,-1},
	{-1,0,0,1,0,1,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,1,1,0,0,1,1,-1},
	{-1,1,1,1,4,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map42[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,2,1,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,0,1,0,0,1,1,-1},
	{-1,0,1,1,1,1,1,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map43[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,1,2,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,1,1,1,0,0,-1},
	{-1,0,1,1,1,0,0,-1},
	{-1,0,1,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,1,4,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map44[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,2,1,1,1,-1},
	{-1,0,0,0,1,1,1,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,1,0,0,1,0,1,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,0,1,1,0,0,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,1,1,1,1,1,4,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map45[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,2,1,1,1,1,1,-1},
	{-1,0,1,1,0,0,0,-1},
	{-1,0,1,1,0,0,0,-1},
	{-1,0,1,0,0,0,0,-1},
	{-1,0,1,0,1,1,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,1,1,1,1,4,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map46[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,2,1,-1},
	{-1,1,0,0,1,0,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,0,1,0,0,1,0,-1},
	{-1,0,1,0,0,1,0,-1},
	{-1,0,0,1,0,0,0,-1},
	{-1,1,0,0,1,1,1,-1},
	{-1,1,1,4,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map47[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,2,1,1,1,1,1,-1},
	{-1,0,0,1,0,0,0,-1},
	{-1,1,0,0,0,1,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,1,1,4,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map48[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,2,1,1,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,1,0,1,0,0,-1},
	{-1,0,0,0,0,0,1,-1},
	{-1,0,0,0,0,1,1,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,4,1,1,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map49[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,2,1,1,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,1,0,1,1,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,1,0,0,0,0,-1},
	{-1,0,0,0,1,1,0,-1},
	{-1,1,1,1,1,1,4,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};

int map50[10][8] = {{-1,-1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,2,1,-1},
	{-1,0,0,0,1,0,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,1,0,0,0,0,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,0,0,0,0,1,0,-1},
	{-1,0,0,0,0,0,0,-1},
	{-1,1,1,4,1,1,1,-1},
	{-1,-1,-1,-1,-1,-1,-1,-1}};


+ (void)initialize{
	
	NSDictionary *appDefaults = [NSDictionary
								 dictionaryWithObjects:[NSArray arrayWithObjects:
														[NSNumber numberWithInt:1],
														[NSNumber numberWithInt:1],
														[NSNumber numberWithInt:3],
														nil]
								 forKeys:[NSArray arrayWithObjects:
										  @"CurrentLevel",
										  @"HighestLevel",
										  @"lives",
										  nil]];

    [[NSUserDefaults standardUserDefaults]
	 registerDefaults:appDefaults];
	
} 

+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	//userInteractionEnables = true;
	
	// 'layer' is an autorelease object.
	progressive *layer = [progressive node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init
{
	
	levelNumber =[[NSUserDefaults standardUserDefaults]
				  integerForKey:@"CurrentLevel"];
	highestLevel =[[NSUserDefaults standardUserDefaults]
				   integerForKey:@"HighestLevel"];
	lives =[[NSUserDefaults standardUserDefaults]
				   integerForKey:@"lives"];
	
	if(levelNumber == 1)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map1[a][b];
	else if(levelNumber == 2)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map_2[a][b];
	else if(levelNumber == 3)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map3[a][b];
	else if(levelNumber == 4)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map4[a][b];
	else if(levelNumber == 5)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map5[a][b];
	else if(levelNumber == 6)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map6[a][b];
	else if(levelNumber == 7)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map7[a][b];
	else if(levelNumber == 8)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map8[a][b];
	else if(levelNumber == 9)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map9[a][b];
	else if(levelNumber == 10)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map10[a][b];
	else if(levelNumber == 11)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map11[a][b];
	else if(levelNumber == 12)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map12[a][b];
	else if(levelNumber == 13)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map13[a][b];
	else if(levelNumber == 14)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map14[a][b];
	else if(levelNumber == 15)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map15[a][b];
	else if(levelNumber == 16)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map16[a][b];
	else if(levelNumber == 17)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map17[a][b];
	else if(levelNumber == 18)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map18[a][b];
	else if(levelNumber == 19)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map19[a][b];
	else if(levelNumber == 20)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map20[a][b];
	else if(levelNumber == 21)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map21[a][b];
	else if(levelNumber == 22)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map22[a][b];
	else if(levelNumber == 23)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map23[a][b];
	else if(levelNumber == 24)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map24[a][b];
	else if(levelNumber == 25)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map25[a][b];
	else if(levelNumber == 26)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map26[a][b];
	else if(levelNumber == 27)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map27[a][b];
	else if(levelNumber == 28)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map28[a][b];
	else if(levelNumber == 29)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map29[a][b];
	else if(levelNumber == 30)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map30[a][b];
	else if(levelNumber == 31)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map31[a][b];
	else if(levelNumber == 32)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map32[a][b];
	else if(levelNumber == 33)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map33[a][b];
	else if(levelNumber == 34)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map34[a][b];
	else if(levelNumber == 35)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map35[a][b];
	else if(levelNumber == 36)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map36[a][b];
	else if(levelNumber == 37)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map37[a][b];
	else if(levelNumber == 38)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map38[a][b];
	else if(levelNumber == 39)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map39[a][b];
	else if(levelNumber == 40)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map40[a][b];
	else if(levelNumber == 41)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map41[a][b];
	else if(levelNumber == 42)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map42[a][b];
	else if(levelNumber == 43)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map43[a][b];
	else if(levelNumber == 44)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map44[a][b];
	else if(levelNumber == 45)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map45[a][b];
	else if(levelNumber == 46)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map46[a][b];
	else if(levelNumber == 47)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map47[a][b];
	else if(levelNumber == 48)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map48[a][b];
	else if(levelNumber == 49)
		for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map49[a][b];
	else for(int a = 0; a < 10; a++)
			for(int b = 0; b < 8; b++)
				thisMap[a][b] = map50[a][b];

	for(int a = 0; a < 10; a++)
		for(int b = 0; b < 8; b++)
		{
			if(thisMap[a][b] == 2)thisMap[a][b] = 4;
			else if(thisMap[a][b] == 4)thisMap[a][b] = 2;
			
		}
	
	
	
	for(int a = 0; a < 10; a++)
		for(int b = 0; b < 8; b++)
		{
			if(thisMap[a][b] == 2){
				endY = a;
			    endX = b;}
		}
	
	
	[self drawMap];
	
	if( (self=[super init] )) {
		CCSprite *background = [CCSprite spriteWithFile:@"main4.png"];
		background.tag = 1;
		background.anchorPoint = CGPointMake(0, 0);
		[self addChild:background];
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:12];
		
		self.isTouchEnabled = true;
		
		
		CCLabel* label = [CCLabel labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:24];
		label.position =  ccp(320-160,480-20);
		label.color = ccc3(0,0,0);
		[self addChild: label];
		[label setString:[NSString stringWithFormat:@"Level %d",levelNumber]];
		
		CCLabel* label2 = [CCLabel labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:24];
		label2.position =  ccp(320-240,20);
		label2.color = ccc3(0,0,0);
		[self addChild: label2];
		[label2 setString:[NSString stringWithFormat:@"Highest level: %d",highestLevel]];

		label3 = [CCLabel labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:24];
		label3.position =  ccp(320-50,20);
		label3.color = ccc3(0,0,0);
		[self addChild: label3];
		[label3 setString:[NSString stringWithFormat:@"lives: %d",lives]];
		
		CCMenuItemImage *item2 = [CCMenuItemImage itemFromNormalImage:@"menu_1.png" selectedImage:@"menu_1.png"
															   target:self
															 selector:@selector(doThis2:)];
		
		item2.position = CGPointMake(320/2-280, 480/2-20);
		
		CCMenuItemImage *item1 = [CCMenuItemImage itemFromNormalImage:@"restart_1.png" selectedImage:@"restart_1.png"
															   target:self
															 selector:@selector(doThis:)];
		
		item1.position = CGPointMake(320/2-50, 480/2-20);
		
		CCMenu *menu1 = [CCMenu menuWithItems:item1,item2, nil];

		[self addChild:menu1];
		[self drawMap];
		
	}
	
	return self;
}

-(void) drawMap
{
	for(int yPosition = 12, a = 0, b = 0, c = 0; a<10;a++, yPosition += 46,b = 0)
		for(int xPosition = -6; b < 8; xPosition += 46, b++, c++)
		{
			if(thisMap[a][b] == 0)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"plain.png"];
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(thisMap[a][b] == 2)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"start.png"];
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(thisMap[a][b] == 5)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"progressiveTile.png"];
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(thisMap[a][b] == 4)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"end.png"];
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
		}
	
	for(int yPosition = 12, a = 0, b = 0, c = 0; a<10;a++,b = 0, yPosition += 46)
		for(int xPosition = -6; b < 8; xPosition += 46, b++, c++)	
		{
			bool left = (thisMap[a][b-1] == 0 || thisMap[a][b-1] == 2), right = (thisMap[a][b+1] == 0 || thisMap[a][b+1] == 2), up = (thisMap[a+1][b] == 0 || thisMap[a+1][b] == 2), down = (thisMap[a-1][b] == 0 || thisMap[a-1][b] == 2);
			bool onPlain = (thisMap[a][b] == 1);
			if(a == 9 && (thisMap[8][b] == 0 || thisMap[8][b] == 2))
			{
				backSprites[a][b] = [CCSprite spriteWithFile:@"4.png"];//down
				backSprites[a][b].position = ccp(xPosition,yPosition);
				[self addChild:backSprites[a][b]];	
			}
			else if(a == 0 && (thisMap[a+1][b] == 0 || thisMap[a+1][b] == 2))
			{
				backSprites[a][b] = [CCSprite spriteWithFile:@"2.png"];//up
				backSprites[a][b].position = ccp(xPosition,yPosition);
				[self addChild:backSprites[a][b]];	
			}
			else if(b == 0 && (thisMap[a][b+1] == 0 || thisMap[a][b+1] == 2))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"3.png"];//right
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(b == 7 && (thisMap[a][b-1] == 0 || thisMap[a][b-1] == 2))
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"1.png"];//left
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			
			else if(onPlain && left && right && up && down)
			{ 
				sprite[a][b] = [CCSprite spriteWithFile:@"13.png"];//center
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(onPlain && left && up && down)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"9.png"];//open right
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(onPlain && left && up && right)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"10.png"];//open down
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(onPlain && up && right && down)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"11.png"];//open left
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(onPlain && left && down && right)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"12.png"];//open up
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(onPlain && left && down)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"5.png"];//open top right
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(onPlain && up && left)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"6.png"];//open right down
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(onPlain && up && right)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"7.png"];//open down left
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(onPlain && down && right)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"8.png"];//open top left
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(onPlain && right && left)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"14.png"];//open up and down
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(onPlain && up && down)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"15.png"];//open left and right
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(onPlain && left)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"1.png"];//closed left
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(onPlain && up)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"2.png"];//close top
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(onPlain && right)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"3.png"];//close right
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			else if(onPlain && down)
			{
				sprite[a][b] = [CCSprite spriteWithFile:@"4.png"];//close down
				sprite[a][b].position = ccp(xPosition,yPosition);
				[self addChild:sprite[a][b]];	
			}
			//else if(b == 7 && sprite[a][b-1]==0)backSprites[a][b] = [CCSprite spriteWithFile:@"1.png"];
			
			
		} 
	
	for(int yPosition = -12, a = 0; a<10 ;a++, yPosition += 46)
		for(int xPosition = -30, b = 0; b < 8; xPosition += 46, b++)	
		{
			rects[a][b] = CGRectMake(xPosition,yPosition,46,46);
		}
	
	
	return;
}

-(bool) isDone
{
		for(int a = 0; a < 8; a++)
			for(int b = 0; b < 10; b++)
				if(thisMap[a][b] == 0 || thisMap[a][b] == 4)return false;

		
		return true;
}

-(void) ccTouchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
	UITouch* touch = [touches anyObject];
	CGPoint location = [touch locationInView: [touch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
	for(int yPosition = 12, a = 0; a < 10 ;a++, yPosition += 46)
		for(int xPosition = -6, b = 0; b < 8;b++, xPosition += 46)
		{
			if(CGRectContainsPoint(rects[a][b], location) && thisMap[a][b] == 2)
			{
			//	sprite[a][b] = [CCSprite spriteWithFile:@"start.png"];
	//			sprite[a][b].position = ccp(xPosition,yPosition);
			//	[self addChild:sprite[a][b]];	
				currentX = b;
				currentY = a;
				onStart = true;
			}
		}
	
	
}

-(void) ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch* touch = [touches anyObject];
	CGPoint location = [touch locationInView: [touch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
	if(CGRectContainsPoint(rects[currentY][currentX-1], location) && onStart && (thisMap[currentY][currentX-1] == 2 || thisMap[currentY][currentX-1] == 4 || thisMap[currentY][currentX-1] == 0))//left
	{
		sprite[currentY][currentX] = [CCSprite spriteWithFile:@"progressiveTile.png"];
		sprite[currentY][currentX].position = ccp((currentX*46)-6,(currentY*46)+12);
		[self addChild:sprite[currentY][currentX]];
		thisMap[currentY][currentX] = 3;
		currentX--;
		thisMap[currentY][currentX] = 2;
		sprite[currentY][currentX] = [CCSprite spriteWithFile:@"start.png"];
		sprite[currentY][currentX].position = ccp((currentX*46)-6,(currentY*46)+12);
		[self addChild:sprite[currentY][currentX]];	
		
	}
	else if(CGRectContainsPoint(rects[currentY][currentX+1], location) && onStart && (thisMap[currentY][currentX+1] == 2 || thisMap[currentY][currentX+1] == 4 || thisMap[currentY][currentX+1] == 0))//right
	{
		sprite[currentY][currentX] = [CCSprite spriteWithFile:@"progressiveTile.png"];
		sprite[currentY][currentX].position = ccp((currentX*46)-6,(currentY*46)+12);
		[self addChild:sprite[currentY][currentX]];
		thisMap[currentY][currentX] = 3;
		currentX++;
		thisMap[currentY][currentX] = 2;
		sprite[currentY][currentX] = [CCSprite spriteWithFile:@"start.png"];
		sprite[currentY][currentX].position = ccp((currentX*46)-6,(currentY*46)+12);
		[self addChild:sprite[currentY][currentX]];	
	}
	else if(CGRectContainsPoint(rects[currentY-1][currentX], location) && onStart && (thisMap[currentY-1][currentX] == 2 || thisMap[currentY-1][currentX] == 4 || thisMap[currentY-1][currentX] == 0))//down
	{
		sprite[currentY][currentX] = [CCSprite spriteWithFile:@"progressiveTile.png"];
		sprite[currentY][currentX].position = ccp((currentX*46)-6,(currentY*46)+12);
		[self addChild:sprite[currentY][currentX]];
		thisMap[currentY][currentX] = 3;
		currentY--;
		thisMap[currentY][currentX] = 2;
		sprite[currentY][currentX] = [CCSprite spriteWithFile:@"start.png"];
		sprite[currentY][currentX].position = ccp((currentX*46)-6,(currentY*46)+12);
		[self addChild:sprite[currentY][currentX]];		
	}
	else if(CGRectContainsPoint(rects[currentY+1][currentX], location) && onStart && (thisMap[currentY+1][currentX] == 2 || thisMap[currentY+1][currentX] == 4 || thisMap[currentY+1][currentX] == 0))//up
	{
		sprite[currentY][currentX] = [CCSprite spriteWithFile:@"progressiveTile.png"];
		sprite[currentY][currentX].position = ccp((currentX*46)-6,(currentY*46)+12);
		[self addChild:sprite[currentY][currentX]];
		thisMap[currentY][currentX] = 3;
		currentY++;
		thisMap[currentY][currentX] = 2;
		sprite[currentY+1][currentX] = [CCSprite spriteWithFile:@"start.png"];
		sprite[currentY+1][currentX].position = ccp((currentX*46)-6,(currentY*46)+12);
		[self addChild:sprite[currentY+1][currentX]];	
	}
		if([self isDone]) doneWithThis = true;
}

-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	if(thisMap[currentY+1][currentX] != -1) onStart = false;
	if([self isDone]){
		[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:levelNumber+1] forKey:@"CurrentLevel"];
		[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:lives] forKey:@"lives"];
		
		if(highestLevel == levelNumber)[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:highestLevel+1] forKey:@"HighestLevel"];
		if(levelNumber > 49)
		{
			[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:1] forKey:@"CurrentLevel"];
			[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:3] forKey:@"lives"];
			[[CCDirector sharedDirector]replaceScene:[CCFadeTransition transitionWithDuration:1.5f scene: [HelloWorld node]]];
		}
		else [[CCDirector sharedDirector]replaceScene:[CCSlideInBTransition transitionWithDuration:1.5f scene: [progressive node]]];
		
	}
}

-(void)doThis:(id)sender{
	
	if(lives == 1)
	{
		[[NSUserDefaults standardUserDefaults] setObject:[NSNumber
														  numberWithInt:1] forKey:@"CurrentLevel"];
		[[NSUserDefaults standardUserDefaults] setObject:[NSNumber
														  numberWithInt:5] forKey:@"lives"];

		[[CCDirector sharedDirector]replaceScene:[CCSlideInTTransition transitionWithDuration:1.5f scene: [progressive node]]];
	}
	else{
	lives--;
	[label3 setString:[NSString stringWithFormat:@"lives: %d",lives]];
	
	for(int a = 0; a < 10; a++)
		for(int b = 0; b < 8; b++)
			if(thisMap[a][b] == 3 || thisMap[a][b] == 2) thisMap[a][b] = 0;
	
	thisMap[endY][endX] = 2;
	
		[self drawMap];}
	
	return;
	
	
}

-(void)doThis2:(id)sender
{
	[[CCDirector sharedDirector]replaceScene:[CCFadeTransition transitionWithDuration:1 scene: [play node]]];
}

@end
