//
//  soundOptions.m
//  Menu
//
//  Created by Joe Malott on 2/20/11.
//  Copyright 2011 0. All rights reserved.
//

#import "help.h"
#import "HelloWorldScene.h"
#import "play.h"
#import "classic.h"
#import "progressive.h"
#import "gmaeOptions.h"
#import "soundOptions.h"
#import "credits.h"


@implementation soundOptions
+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	//userInteractionEnables = true;
	
	// 'layer' is an autorelease object.
	soundOptions *layer = [soundOptions node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init
{
	if( (self=[super init] )) {
		CCSprite* background = [CCSprite spriteWithFile:@"main.png"];
		background.tag = 1;
		background.anchorPoint = CGPointMake(0, 0);
		[self addChild:background];
	}
	CCLabelTTF* label4 = [CCLabelTTF labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:50];
	label4.position =  ccp(320-160,480/2-120);
	label4.color = ccc3(0,0,0);
	[self addChild: label4];
	[label4 setString:@"soundsOptions"];
	self.isTouchEnabled = YES;
	
	return self;
	
}

@end
