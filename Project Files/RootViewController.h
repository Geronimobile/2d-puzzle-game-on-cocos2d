//
//  RootViewController.h
//  rubLite
//
//  Created by Joe Malott on 3/1/11.
//  Copyright 2011 0. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <iAd/iAd.h>
#import “cocos2d.h”
@interface RootViewController : UIViewController<ADBannerViewDelegate> {
	ADBannerView *bannerView;
}
@property (nonatomic,retain) ADBannerView *bannerView;
@end



