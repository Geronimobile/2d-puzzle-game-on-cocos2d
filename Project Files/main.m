//
//  main.m
//  Menu
//
//  Created by Greg Roberts on 8/21/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
	NSAutoreleasePool *pool = [NSAutoreleasePool new];
	int retVal = UIApplicationMain(argc, argv, nil, @"MenuAppDelegate");
	[pool release];
	return retVal;
}
